import React, { useEffect } from "react";
import { connect } from "react-redux";
import LogItem from "./LogItem";
import Preloader from "../layout/Preloader";
import PropTypes from "prop-types";
import { getLogs } from "../../actions/logActions";

const Logs = ({ log: { logs, loading }, getLogs }) => {
  useEffect(() => {
    getLogs();
    // eslint-disable-next-line
  }, []);

  if (loading || logs === null) {
    return <Preloader />;
  }

  return (
    <ul className="collection with-header">
      <li className="collection-header">
        <h4 className="center">System Logs</h4>
      </li>
      {!loading && logs.length === 0 ? (
        <p className="center">No logs to show...</p>
      ) : (
        logs.map((log) => <LogItem log={log} key={log.id} />)
      )}
    </ul>
  );
};

Logs.propTypes = {
  log: PropTypes.object.isRequired,
  getLogs: PropTypes.func.isRequired,
};

// log - это props, state.log - это из рутредюсера log - т.е logReducer
//Можно как в log: state.log (только сверху в компонент деструктурировать log, который уже приходит из state),
// а можно просто вытащить как в loading: state.log.loading.
// Т.е в mapStateToProps - мы пихаем в придуманный нами параметр log - пихаем state.log - т.е весь initalState с logReducer и его
// прокидываем вверх в параметры компонента, как props
// getLogs забираем также сверху, но он идет с actions
const mapStateToProps = (state) => ({
  log: state.log,
  //loading: state.log.loading,
});

//Второй параметр - это объект с actions, который уже мапим actions to props, т.е закидываем в props уже как action
export default connect(mapStateToProps, { getLogs })(Logs);
